class Question < ActiveRecord::Base
  attr_accessible :text, :poll_id

  validates :text, :poll_id, presence: true

  belongs_to(
    :poll,
    class_name: "Poll",
    foreign_key: :poll_id,
    primary_key: :id
  )

  has_many(
    :answer_choices,
    class_name: "AnswerChoice",
    foreign_key: :question_id,
    primary_key: :id
  )

  def response_count_query
    # bindings = [self.id]
    # query = <<-SQL
    # SELECT
    #   ac.text, COUNT(r.*)
    # FROM
    #   questions q
    # JOIN answer_choices ac
    #   ON q.id = ac.question_id
    #       OUTER JOIN responses r
    #       ON ac.id = r.answer_choice_id
    # WHERE
    #   q.id = ?
    # GROUP BY ac.text
    # SQL
    #
    # bindings.unshift(query)
    #
    # self.class.find_by_sql(bindings)

    posts_with_counts = self
      .answer_choices
      .select("answer_choices.text, COUNT(responses.*)") #AS reply_count"
      .joins("LEFT OUTER JOIN responses ON answer_choices.id = responses.answer_choice_id")
      .group("answer_choices.text")

    posts_with_counts
  end


  def response_count
    response_hash = {}
    response_count_query.each do |obj|
      response_hash[obj.text] = obj.count.to_i
    end
    response_hash
  end

end