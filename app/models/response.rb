class Response < ActiveRecord::Base
  attr_accessible :chooser_id, :answer_choice_id

  validates :chooser_id, :answer_choice_id, presence: true
  validate  :respondent_has_not_already_answered_question
  # validate  :respondent_is_not_poll_author

  belongs_to(
    :chooser,
    class_name: "User",
    foreign_key: :chooser_id,
    primary_key: :id
  )

  belongs_to(
    :answer_choice,
    class_name: "AnswerChoice",
    foreign_key: :answer_choice_id,
    primary_key: :id
  )

  def existing_responders
    bindings = [self.chooser_id, self.answer_choice_id]
    query = <<-SQL
      SELECT
        r.chooser_id
      FROM
        responses r
      JOIN
        answer_choices ac1 ON ac1.id = r.answer_choice_id
      WHERE r.chooser_id = ? AND ac1.question_id =
      (SELECT
        q.id
      FROM
        questions q
      JOIN answer_choices ac2 ON q.id = ac2.question_id
      WHERE
        ac2.id = ?)
    SQL
    bindings.unshift(query)

    self.class.find_by_sql(bindings)

  end

  def respondent_has_not_already_answered_question
    id_arr = []
    existing_responders.each {|el| id_arr << el.chooser_id}
    if id_arr.count > 0 && id_arr.include?(self.chooser_id)
      errors[:answer_choice_id] << "can't answer same question twice"
    end
  end


  def author_for_ac
    binding = [self.answer_choice_id]
    query = <<-SQL
      SELECT
        DISTINCT p.*
      FROM
        responses r
        JOIN answer_choices ac ON r.answer_choice_id = ac.id
             JOIN questions q  ON ac.question_id = q.id
                  JOIN polls p ON q.poll_id = p.id
      WHERE r.answer_choice_id = ?
    SQL
    binding.unshift(query)

    self.class.find_by_sql(binding)
  end

  def respondent_is_not_poll_author
    poll_author = author_for_ac.first.author_id.to_i
    if poll_author == self.chooser_id
      errors[:answer_choice_id] << "can't answer question for your own poll"
    end
  end

end