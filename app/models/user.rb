class User < ActiveRecord::Base
  attr_accessible :user_name

  validates :user_name, presence: true, uniqueness: true

  has_many(
    :polls,
    class_name: "Poll",
    foreign_key: :author_id,
    primary_key: :id
  )

  has_many(
    :responses,
    class_name: "Response",
    foreign_key: :chooser_id,
    primary_key: :id
  )

  def completed_polls

  end

  def uncompleted_polls

  end

end